#/bin/sh
echo changing $1 to $2
sed -i '' "s/$1/$2/g" *.txt *.html
git commit -am "$1 -> $2"
