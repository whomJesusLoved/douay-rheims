#!/bin/sh

if [ $# -eq 0 ]; then
    echo "Syntax: $0 '<book chapter:verse>' ['<end book chapter:verse>']"
    exit 1;
fi

# use a trailing . for the verse as the format includes that and thus we can
# distinguish between 1 and 10, .. 19 when asking for verse 1.
if [ $# -gt 1 ]; then
    if [ "$DEBUG" == "1" ]; then
        echo "Showing verses $1 to $2"
    fi
    sed -n "/^$1\./,/^$2\./p" bible.txt | sed '$d'
    shift
fi

if [ "$DEBUG" == "1" ]; then
        echo "Showing verse $1"
fi
sed -n "/^$1\./,/^$/p" bible.txt
exit 0;
